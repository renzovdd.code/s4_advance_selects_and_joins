-- 2.A - Find all artists that has letter D in its name
SELECT * FROM artists WHERE name LIKE '%D%';

-- 2.B Find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < '00:02:00';

-- 2.C Join the albums and songs tables. (Only show the album name, song name, and song length)
SELECT album_name, title, length FROM albums JOIN songs ON albums.id = songs.album_id; 

-- 2.D Join the artists and albums table. Find all the albums that has letter A in its name. 
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_name LIKE '%A%';

-- 2.e Sort the albums in Z-A order. Show only the first 4 records
SELECT * FROM albums ORDER BY album_name DESC LIMIT 4;

-- 2.f Join the albums and songs tables. Sort albums from z-a and songs from a-z
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_name DESC, title ASC;

