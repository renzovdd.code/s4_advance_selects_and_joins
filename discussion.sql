-- Task 1:
INSERT INTO artists(name)
VALUES ('Taylor Swift');

INSERT INTO artists(name)
VALUES ('Lady Gaga');

INSERT INTO artists(name)
VALUES ('Justin Bieber');

INSERT INTO artists(name)
VALUES ('Ariana Grande');

INSERT INTO artists(name)
VALUES ('Bruno Mars');

-- Task 2:

INSERT INTO artists(name)
VALUES 
('Third Eye Blind'),
('Nsync'),
('Tracy Chapman'),
('Sublime'),
('All Saints');

-- Task 3
INSERT INTO albums(album_name, year, artist_id)
VALUES
('Fearless',2008-01-01,'3'),
('Red',2012-01-01,'3'),
('A Star is Born',2018-01-01,'4'),
('Born This Way',2011-01-01,'4'),
('Purpose',2015-01-01,'5'),
('Believe',2012-01-01,'5'),
('Dangerous Woman',2016-01-01,'6'),
('Thank U, Next',2019-01-01,'6'),
('24 Magic',2016-01-01,'7'),
('Earth to Mars',2011-01-01,'7');

-- minitask
INSERT INTO songs(title, length ,genre, album_id)
VALUES
('Fearless',264 ,'POP',5),
('Love Story',213,'POP',5),
('State of Grace',273,'POP',6),
('Red',204,'POP',6);

-- Advance selects in MYSQL
-- Exclude Records [!=]
SELECT * FROM artists WHERE id != 11;
-- This will exlude the artist with an id = 11
SELECT * FROM albums WHERE id = 2 OR id = 31 OR id 
-- TO GET SPECIFIC DATA using IN expression
SELECT * FROM albums WHERE artist_id IN (6);

INSERT INTO artists(name)
VALUES 
('John Mayer'),
('John Lennon'),
('Adele'),
('Khalid'),
('Drake');

UPDATE albums
SET year = '2003-01-01'
WHERE id = 15;

INSERT INTO albums(album_name, year, artist_id)
VALUES
('Paradise Valley',2003-01-01,'13'),
('Room of Squares',2001-01-01,'13'),
('Imagine',1971-01-01,'14'),
('Abbey Road',1969-01-01,'14'),
('25',2015-01-01,'15'),
('21',2011-01-01,'15'),
('Free Spirit',2019-01-01,'16'),
('Suncity',2018-01-01,'16'),
('Scorpion',2018-01-01,'17'),
('Views',2016-01-01,'17');

INSERT INTO songs(title, length, genre, album_id)
VALUES
('Wildfire','415','FOLK ROCK','15'),
('Dear Marie','344','FOLK ROCK','15'),
('Paper Doll','419','FOLK ROCK','15'),

('Neon','423','SOFT ROCK','16'),
('City Love','403','SOFT ROCK','16'),
('Back to You','404','SOFT ROCK','16'),

('Imagine','301','ROCK','17'),
('How?','343','ROCK','17'),
('Oh My Love','420','ROCK','17'),

('Come Together','419','ROCK','18'),
('Something','302','ROCK','18'),
('Because','245','ROCK','18'),

('Hello','455','SOUL','19'),
('Send My Love','343','SOUL','19'),
('All I Ask','432','SOUL','19'),

('Rolling in the Deep','349','SOUL','20'),
('Rumour Has It','343','SOUL','20'),
('One and Only','548','SOUL','20'),

('Talk','317','R&B','21'),
('Better','349','R&B','21'),
('Saturday Nights','329','R&B','21'),

('Vertigo','430','R&B','22'),
('Motion','355','R&B','22'),
('OTW','423','R&B','22'),

("God's Plan",'319','R&B','23'),
('Elevate','304','R&B','23'),
('Nonstop','358','R&B','23'),

('Views','511','R&B','24'),
('One Dance','254','R&B','24'),
('Too Good','423','R&B','24');

-- Section TABLE JOINS
-- JOIN CLAUSE is useed to combine rows from 2 or more tables, based on a related column between them.

-- ON CLAUSE -- this is used to specify a JOIN condition.
-- which lets us specify and separate the conditions upon combining the selected tables. 

-- combine the artists and albums table.
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- COMBINE MORE THAN 2 TABLES
-- artists, albums, songs
-- 1. select all the data from the desired tables
-- 2. USE the JOIN clause to combine the desired tables.
-- 3. Create a condition that will serve as a filter in order to specify a set of requirements that would assess the realted values or columns amongst the tables. 
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;
-- Other solution
 SELECT * FROM artists, albums, songs WHERE artists.id = albums.artist_id AND albums.id = songs.album_id;

--  USING THE JOIN CLAUSE we will select columns to be indluded per table.
-- TASK: What if you only want to display the name of the artist and Title of the album 
-- 1. SELECT the tables which you want to gather the data.
-- 2. Identify what information you want to extract on each table.  
SELECT artists.name, albums.album_name FROM artists JOIN albums ON artists.id = albums.artist_id;